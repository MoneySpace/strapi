module.exports = ({ env }) => ({
  host: env('HOST', '0.0.0.0'),
  port: env.int('PORT', 1337),
  url: 'http://172.104.207.207/api',
  admin: {
    auth: {
      secret: env('ADMIN_JWT_SECRET', '6e9bf617699925ece2d9ea9f6623e736'),
    },
    // url: 'http://172.104.207.207/dashboard'
  },
});
