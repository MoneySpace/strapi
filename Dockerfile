FROM node:14.11.0
WORKDIR /app
ENV NODE_ENV=production
ADD package.json .
RUN yarn install
ADD . .
RUN NODE_OPTIONS=--max_old_space_size=3072 yarn build
EXPOSE 1337
CMD ["yarn", "start"]
